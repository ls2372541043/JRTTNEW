package com.example.dell.newtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioGroup;

import com.example.dell.newtest.fragment.BaseFragment;
import com.example.dell.newtest.fragment.NewsChanneFragment;
import com.example.dell.newtest.fragment.PicFragment;
import com.example.dell.newtest.fragment.UserFragment;
import com.example.dell.newtest.fragment.VideoFragment;


import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    HashMap<Integer,BaseFragment> pages = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pages.put(R.id.radio1,new NewsChanneFragment());
        pages.put(R.id.radio2,new VideoFragment());
        pages.put(R.id.radio3,new PicFragment());
        pages.put(R.id.radio4,new UserFragment());

        //RadioGroup radioGroup = findViewById(R.id.radio_group);
        RadioGroup radioGroup = findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                //由radioGroup可以获取被点击的按钮id
                int id = radioGroup.getCheckedRadioButtonId();
                //在集合里找id
                BaseFragment page = pages.get(id);
                //获取FragmentManager用来添加Fragment到布局
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame,page)
                        .commit();//只有commit才能显示页面

            }
        });
        //在监听器配置好的情况下，默认选中其中一个页面
        //模拟人的点击
        radioGroup.check(R.id.radio1);
    }
}

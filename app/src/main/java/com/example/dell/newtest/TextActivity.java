package com.example.dell.newtest;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.dell.newtest.fragment.PicFragment;

public class TextActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        Fragment picFragment = new PicFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.frame,picFragment).commit();//
    }
}

package com.example.dell.newtest;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.VideoView;

public class SplahActivity extends AppCompatActivity {

    private boolean isClickClose = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splah);
        //1,复制一个视频到res/raw文件夹
        //2,视频的路径表示
        String mp4 = "android.resource://"+getPackageName()+"/"+R.raw.start;
        Uri uri = Uri.parse(mp4);
        //3,先在Layout布局文件中配置一个videoview，再找出来
        VideoView videoView = (VideoView) findViewById(R.id.video_view);
        setFullWidth(videoView);
        


        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                isClickClose = true;//表示你是点击按钮关闭页面
                Intent intent = new Intent(SplahActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        videoView.setVideoURI(uri);

        videoView.start();

        findViewById(R.id.btn_jump).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClickClose){
                    //什么都不做
                }else {
                    Intent intent = new Intent(SplahActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });


    }

    public void setFullWidth(VideoView videoView) {
        //如果视频画面没有占满屏幕，只能重新计算屏幕宽高
        //metrics标尺
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        //创建LayoutParams：用代码来设置宽高的对象
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
        //再设置给videoview
        videoView.setLayoutParams(params);
    }
}

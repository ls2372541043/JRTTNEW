package com.example.dell.newtest.net;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by dell on 2020/6/30.
 */

public interface JrttApi {
    @GET("photos/photos_1.json")
    Call<ResponseData> getDataPic();//解析json
}

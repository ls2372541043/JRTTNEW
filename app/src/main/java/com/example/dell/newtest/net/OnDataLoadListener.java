package com.example.dell.newtest.net;

/**
 * Created by dell on 2020/6/30.
 */

public interface OnDataLoadListener {
    void initViews();
    void requestNet();
    void parsejson(String json);
    void showData();
}

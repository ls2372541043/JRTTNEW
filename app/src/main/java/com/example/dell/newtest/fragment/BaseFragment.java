package com.example.dell.newtest.fragment;

import android.support.v4.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dell.newtest.R;

/**
 * Created by dell on 2020/6/30.
 */

public class BaseFragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getContext(), R.layout.fragment_home,null);

        TextView textView = (TextView) view;


       textView.setBackgroundColor(Color.GRAY);
       textView.setText(getClass().getSimpleName());
        return view;
    }
}

package com.example.dell.newtest.adapter;



import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.dell.newtest.R;
import com.example.dell.newtest.bean.PicData;
import com.joanzapata.android.BaseAdapterHelper;
import com.joanzapata.android.QuickAdapter;

import java.util.List;

/**
 * Created by dell on 2020/6/30.
 */


public class PicQuickAdapter extends QuickAdapter<PicData.NewsBean> {


    public PicQuickAdapter(Context context, int layoutResId, List<PicData.NewsBean> data) {
        super(context, layoutResId, data);

    }


    @Override
    protected void convert(BaseAdapterHelper helper, PicData.NewsBean item) {

        //显示文字
        helper.setText(R.id.text,item.title);

        ImageView imageview =  helper.getView(R.id.image);
        //图片一会再来处理
        //网址加载图片
        Glide.with(context).load(item.listimage).into(imageview);
    }
}

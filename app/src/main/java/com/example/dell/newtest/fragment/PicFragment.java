package com.example.dell.newtest.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.dell.newtest.R;
import com.example.dell.newtest.adapter.PicQuickAdapter;
import com.example.dell.newtest.bean.PicData;
import com.example.dell.newtest.net.JrttApi;
import com.example.dell.newtest.net.OnDataLoadListener;
import com.example.dell.newtest.net.ResponseData;
import com.example.dell.newtest.net.RetrofitUtils;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import android.content.Context;




/**
 * Created by dell on 2020/6/30.
 */

public class PicFragment extends BaseFragment implements OnDataLoadListener{
    private ListView listview;
    private View view;
    private PicData picData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        initViews();
        requestNet();
        return view;

    }

    @Override
    public void initViews() {
        //1,到布局fragment_pic 里面添加一个 <ListView>
        view = View.inflate(getContext(),R.layout.fragment_pic,null);
        //view = View.inflate(getContext()), R.layout.fragment_pic,null);
        //2,回到PicFragment查找View
        listview = view.findViewById(R.id.listview);
    }

    @Override
    public void requestNet() {

        // 3,获取服务端的数据
        //http://localhost:8080/jrtt/photos/photos_1.json

        JrttApi api = RetrofitUtils.get();
        Call<ResponseData> call = api.getDataPic();
        RetrofitUtils.send(getContext(),call,PicFragment.this);
    }

    @Override
    public void parsejson(String json) {

        // 4，将json转成集合
        //解析json数据使用gson库
        Gson gson = new Gson();
        picData = gson.fromJson(json, PicData.class);
        for (PicData.NewsBean item : picData.news) {


            Toast.makeText(getContext(), item.title + "" + item.listimage, Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void showData() {

            //5, 创建一个adapter
            PicQuickAdapter adapter = new PicQuickAdapter(getContext(),R.layout.item_pic,picData.news);
            // 6, 设置给ListView
            listview.setAdapter(adapter);
    }
}
